# Danish translation of release-notes for Debian 8.
# Copyright (C) 2011 Free Software Foundation, Inc.
# Joe Hansen <joedalton2@yahoo.dk>, 2011, 2013, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: release-notes 8.0 about.po\n"
"POT-Creation-Date: 2021-05-04 19:18+0200\n"
"PO-Revision-Date: 2014-11-18 20:09+0100\n"
"Last-Translator: Joe Hansen <joedalton2@yahoo.dk>\n"
"Language-Team: Danish <debian-l10n-danish@lists.debian.org>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Attribute 'lang' of: <chapter>
#: en/about.dbk:8
msgid "en"
msgstr "da"

#. type: Content of: <chapter><title>
#: en/about.dbk:9
msgid "Introduction"
msgstr "Introduktion"

#. type: Content of: <chapter><para>
#: en/about.dbk:11
msgid ""
"This document informs users of the &debian; distribution about major changes "
"in version &release; (codenamed &releasename;)."
msgstr ""
"Dette dokument informerer brugere af &debian;-distributionen om større "
"ændringer i version &release; (kodenavn &releasename;)."

#. type: Content of: <chapter><para>
#: en/about.dbk:15
msgid ""
"The release notes provide information on how to upgrade safely from release "
"&oldrelease; (codenamed &oldreleasename;) to the current release and inform "
"users of known potential issues they could encounter in that process."
msgstr ""
"Udgivelsesnoterne har information om, hvordan du sikkert opgraderer fra "
"version &oldrelease; (kodenavn &oldreleasename;) til den aktuelle udgave og "
"informerer brugere om kendte problemstillinger, som kan opstå under "
"opgraderingen."

#. type: Content of: <chapter><para>
#: en/about.dbk:21
#, fuzzy
#| msgid ""
#| "You can get the most recent version of this document from <ulink url="
#| "\"&url-release-notes;\"></ulink>.  If in doubt, check the date on the "
#| "first page to make sure you are reading a current version."
msgid ""
"You can get the most recent version of this document from <ulink url=\"&url-"
"release-notes;\"></ulink>."
msgstr ""
"Du kan se den seneste version af dette dokument på <ulink url=\"&url-release-"
"notes;\"></ulink>. Du kan sikre dig, at du læser den seneste version af "
"dokumentet ved at tjekke datoen på den første side."

#. type: Content of: <chapter><caution><para>
#: en/about.dbk:26
msgid ""
"Note that it is impossible to list every known issue and that therefore a "
"selection has been made based on a combination of the expected prevalence "
"and impact of issues."
msgstr ""
"Bemærk at det er umuligt at skrive om alle kendte problemstillinger, og at "
"udvælgelsen er baseret på en kombination af forventet forekomst og omfang."

#. type: Content of: <chapter><para>
#: en/about.dbk:32
msgid ""
"Please note that we only support and document upgrading from the previous "
"release of Debian (in this case, the upgrade from &oldreleasename;).  If you "
"need to upgrade from older releases, we suggest you read previous editions "
"of the release notes and upgrade to &oldreleasename; first."
msgstr ""
"Bemærk at vi alene understøtter og dokumenterer opgradering fra den forrige "
"version af Debian (i dette tilfælde, opgradering fra &oldreleasename;). Hvis "
"du har brug for at opgradere fra en ældre version, foreslår vi, at du læser "
"tidligere udgaver af udgivelsesnoterne og først opgraderer til "
"&oldreleasename;."

#. type: Content of: <chapter><section><title>
#: en/about.dbk:40
msgid "Reporting bugs on this document"
msgstr "Rapporter fejl i dette dokument"

#. type: Content of: <chapter><section><para>
#: en/about.dbk:42
msgid ""
"We have attempted to test all the different upgrade steps described in this "
"document and to anticipate all the possible issues our users might encounter."
msgstr ""
"Vi har forsøgt at teste alle trin i opgraderingen, som beskrives i det her "
"dokument og at forudse alle de mulige problemstillinger, som en bruger kan "
"møde."

#. type: Content of: <chapter><section><para>
#: en/about.dbk:47
#, fuzzy
#| msgid ""
#| "Nevertheless, if you think you have found a bug (incorrect information or "
#| "information that is missing)  in this documentation, please file a bug in "
#| "the <ulink url=\"&url-bts;\">bug tracking system</ulink> against the "
#| "<systemitem role=\"package\">release-notes</systemitem> package. You "
#| "might want to review first the <ulink url=\"&url-bts-rn;\">existing bug "
#| "reports</ulink> in case the issue you've found has already been reported. "
#| "Feel free to add additional information to existing bug reports if you "
#| "can contribute content for this document."
msgid ""
"Nevertheless, if you think you have found a bug (incorrect information or "
"information that is missing)  in this documentation, please file a bug in "
"the <ulink url=\"&url-bts;\">bug tracking system</ulink> against the "
"<systemitem role=\"package\">release-notes</systemitem> package. You might "
"first want to review the <ulink url=\"&url-bts-rn;\">existing bug reports</"
"ulink> in case the issue you've found has already been reported. Feel free "
"to add additional information to existing bug reports if you can contribute "
"content for this document."
msgstr ""
"Alligevel opstår fejl og hvis du mener, at du har fundet en sådan (forkert "
"information eller information som mangler) i denne dokumentation, så indsend "
"venligst en fejlrapport her <ulink url=\"&url-bts;"
"\">fejlrapporteringssystemet</ulink> mod pakken <systemitem role=\"package"
"\">release-notes</systemitem>. Du bør først kontrollere de <ulink url=\"&url-"
"bts-rn;\">eksisterende fejlrapporter</ulink> for at sikre dig, at fejlen "
"ikke allerede er rapporteret. Du kan frit tilføje yderligere information til "
"en eksisterende fejlrapport, hvis du kan bidrage med indhold til dette "
"dokument."

# opmuntre, opfordre
#. type: Content of: <chapter><section><para>
#: en/about.dbk:59
msgid ""
"We appreciate, and encourage, reports providing patches to the document's "
"sources. You will find more information describing how to obtain the sources "
"of this document in <xref linkend=\"sources\"/>."
msgstr ""
"Vi er taknemlige for og opfordrer til fejlrettelser til dokumentets kilder, "
"som er vedhæftet fejlrapporten. Du kan finde yderligere information, der "
"beskriver hvordan du kan finde kilderne til dette dokument, i <xref linkend="
"\"sources\"/>."

#. type: Content of: <chapter><section><title>
#: en/about.dbk:67
msgid "Contributing upgrade reports"
msgstr "Bidrag med opgraderingsrapporter"

#. type: Content of: <chapter><section><para>
#: en/about.dbk:69
msgid ""
"We welcome any information from users related to upgrades from "
"&oldreleasename; to &releasename;.  If you are willing to share information "
"please file a bug in the <ulink url=\"&url-bts;\">bug tracking system</"
"ulink> against the <systemitem role=\"package\">upgrade-reports</systemitem> "
"package with your results.  We request that you compress any attachments "
"that are included (using <command>gzip</command>)."
msgstr ""
"Vi er glade for al information fra brugere, som har forbindelse til "
"opgraderinger fra &oldreleasename; til &releasename;. Hvis du vil dele din "
"information med os, så kan du sende denne ind via en fejlrapport i <ulink "
"url=\"&url-bts;\">fejlrapporteringssystemet</ulink> mod pakken <systemitem "
"role=\"package\">upgrade-reports</systemitem> med dine erfaringer. Vi vil "
"bede dig om, at du komprimerer eventuelle bilag som inkluderes (med "
"<command>gzip</command>)."

#. type: Content of: <chapter><section><para>
#: en/about.dbk:78
msgid ""
"Please include the following information when submitting your upgrade report:"
msgstr ""
"Inkluder følgende information når du indsender din opgraderingsrapport:"

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/about.dbk:85
msgid ""
"The status of your package database before and after the upgrade: "
"<systemitem role=\"package\">dpkg</systemitem>'s status database available "
"at <filename>/var/lib/dpkg/status</filename> and <systemitem role=\"package"
"\">apt</systemitem>'s package state information, available at <filename>/var/"
"lib/apt/extended_states</filename>.  You should have made a backup before "
"the upgrade as described at <xref linkend=\"data-backup\"/>, but you can "
"also find backups of <filename>/var/lib/dpkg/status</filename> in <filename>/"
"var/backups</filename>."
msgstr ""
"Status på din pakkedatabase før og efter opgraderingen: <systemitem role="
"\"package\">dpkg</systemitem>'s statusdatabase tilgængelig i <filename>/var/"
"lib/dpkg/status</filename> og <systemitem role=\"package\">apt</"
"systemitem>'s pakketilstandsinformation, tilgængelig i <filename>/var/lib/"
"apt/extended_states</filename>. Du bør lave en sikkerhedskopi før "
"opgraderingen som beskrevet i <xref linkend=\"data-backup\"/>, men du kan "
"også finde sikkerhedskopier af <filename>/var/lib/dpkg/status</filename> i "
"<filename>/var/backups</filename>."

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/about.dbk:98
msgid ""
"Session logs created using <command>script</command>, as described in <xref "
"linkend=\"record-session\"/>."
msgstr ""
"Sessionslog fra <command>script</command>, læs mere om dette i <xref linkend="
"\"record-session\"/>."

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/about.dbk:104
msgid ""
"Your <systemitem role=\"package\">apt</systemitem> logs, available at "
"<filename>/var/log/apt/term.log</filename>, or your <command>aptitude</"
"command> logs, available at <filename>/var/log/aptitude</filename>."
msgstr ""
"Dine <systemitem role=\"package\">apt</systemitem>-logge, tilgængelige i "
"<filename>/var/log/apt/term.log</filename> eller dine <command>aptitude</"
"command>-logge tilgængelige i <filename>/var/log/aptitude</filename>."

#. type: Content of: <chapter><section><note><para>
#: en/about.dbk:113
msgid ""
"You should take some time to review and remove any sensitive and/or "
"confidential information from the logs before including them in a bug report "
"as the information will be published in a public database."
msgstr ""
"Du bør gennemgå og fjerne al personlig og/eller fortrolig information fra "
"logge, før du inkluderer dem i en fejlrapport, da informationen vil blive "
"udgivet i en offentlig database."

#. type: Content of: <chapter><section><title>
#: en/about.dbk:122
msgid "Sources for this document"
msgstr "Kilder til dette dokument"

#. type: Content of: <chapter><section><para>
#: en/about.dbk:124
#, fuzzy
#| msgid ""
#| "The source of this document is in DocBook XML<indexterm><primary>DocBook "
#| "XML</primary></indexterm> format. The HTML version is generated using "
#| "<systemitem role=\"package\">docbook-xsl</systemitem> and <systemitem "
#| "role=\"package\">xsltproc</systemitem>. The PDF version is generated "
#| "using <systemitem role=\"package\">dblatex</systemitem> or <systemitem "
#| "role=\"package\">xmlroff</systemitem>. Sources for the Release Notes are "
#| "available in the SVN repository of the <emphasis>Debian Documentation "
#| "Project</emphasis>.  You can use the <ulink url=\"&url-vcs-release-notes;"
#| "\">web interface</ulink> to access its files individually through the web "
#| "and see their changes.  For more information on how to access the SVN "
#| "please consult the <ulink url=\"&url-ddp-vcs-info;\">Debian Documentation "
#| "Project SVN information pages</ulink>."
msgid ""
"The source of this document is in DocBook XML<indexterm><primary>DocBook "
"XML</primary></indexterm> format. The HTML version is generated using "
"<systemitem role=\"package\">docbook-xsl</systemitem> and <systemitem role="
"\"package\">xsltproc</systemitem>. The PDF version is generated using "
"<systemitem role=\"package\">dblatex</systemitem> or <systemitem role="
"\"package\">xmlroff</systemitem>. Sources for the Release Notes are "
"available in the Git repository of the <emphasis>Debian Documentation "
"Project</emphasis>.  You can use the <ulink url=\"&url-vcs-release-notes;"
"\">web interface</ulink> to access its files individually through the web "
"and see their changes.  For more information on how to access Git please "
"consult the <ulink url=\"&url-ddp-vcs-info;\">Debian Documentation Project "
"VCS information pages</ulink>."
msgstr ""
"Kilden til dette dokument er i formatet DocBook "
"XML<indexterm><primary>DocBook XML</primary></indexterm>. HTML-versionen er "
"oprettet med <systemitem role=\"package\">docbook-xsl</systemitem> og "
"<systemitem role=\"package\">xsltproc</systemitem>. PDF-versionen er "
"oprettet med <systemitem role=\"package\">dblatex</systemitem> eller "
"<systemitem role=\"package\">xmlroff</systemitem>. Kilder for "
"udgivelsesnoterne er tilgængelige i SVN-arkivet for <emphasis>Debian "
"Documentation Project</emphasis>. Du kan anvende <ulink url=\"&url-svn-"
"release-notes;\">internetbrugerfladen</ulink> for at tilgå disse filer "
"individuelt via internettet og se ændringer i dem. For yderligere "
"information om hvordan SVN tilgås, så læs <ulink url=\"&url-ddp-vcs-info;"
"\">SVN-siderne for Debian Documentation Project</ulink>."

#~ msgid "TODO: any more things to add here?\n"
#~ msgstr "TODO: any more things to add here?\n"
