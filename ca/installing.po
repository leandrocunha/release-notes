# Catalan translation of the Debian Release Notes
#
# Miguel Gea Milvaques, 2006-2009.
# Jordà Polo, 2007, 2009.
# Guillem Jover <guillem@debian.org>, 2007, 2019.
# Héctor Orón Martínez, 2011.
#
msgid ""
msgstr ""
"Project-Id-Version: release-notes 10\n"
"POT-Creation-Date: 2021-03-27 22:34+0100\n"
"PO-Revision-Date: 2020-12-09 15:08+0100\n"
"Last-Translator: Alex Muntada <alexm@debian.org>\n"
"Language-Team: Catalan <debian-l10n-catalan@lists.debian.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.2.1\n"

#. type: Attribute 'lang' of: <chapter>
#: en/installing.dbk:8
msgid "en"
msgstr "ca"

#. type: Content of: <chapter><title>
#: en/installing.dbk:9
msgid "Installation System"
msgstr "El sistema d'instal·lació"

#. type: Content of: <chapter><para>
#: en/installing.dbk:11
msgid ""
"The Debian Installer is the official installation system for Debian.  It "
"offers a variety of installation methods.  Which methods are available to "
"install your system depends on your architecture."
msgstr ""
"El sistema d'instal·lació oficial a Debian és el Debian Installer. Ens "
"proporciona diversos mètodes d'instal·lació. Els mètodes dels que disposeu "
"per instal·lar el vostre sistema dependran de la vostra arquitectura."

#. type: Content of: <chapter><para>
#: en/installing.dbk:16
msgid ""
"Images of the installer for &releasename; can be found together with the "
"Installation Guide on the <ulink url=\"&url-installer;\">Debian website</"
"ulink>."
msgstr ""
"Les imatges de l'instal·lador de &releasename; es poden trobar junt a la "
"guia d'instal·lació a la <ulink url=\"&url-installer;\">web de Debian </"
"ulink>."

#. type: Content of: <chapter><para>
#: en/installing.dbk:21
msgid ""
"The Installation Guide is also included on the first media of the official "
"Debian DVD (CD/blu-ray) sets, at:"
msgstr ""
"La guia d'instal·lació també està inclosa al primer suport dels conjunts de "
"DVD de Debian (CD/blu-ray), a:"

#. type: Content of: <chapter><screen>
#: en/installing.dbk:25
#, no-wrap
msgid "/doc/install/manual/<replaceable>language</replaceable>/index.html\n"
msgstr "/doc/install/manual/<replaceable>ca</replaceable>/index.html\n"

#. type: Content of: <chapter><para>
#: en/installing.dbk:28
msgid ""
"You may also want to check the <ulink url=\"&url-installer;index#errata"
"\">errata</ulink> for debian-installer for a list of known issues."
msgstr ""
"Pot ser també voldreu comprovar les <ulink url=\"&url-installer;index#errata"
"\">errates</ulink> del debian-installer per obtenir la llista de problemes "
"coneguts."

#. type: Content of: <chapter><section><title>
#: en/installing.dbk:33
msgid "What's new in the installation system?"
msgstr "Què hi ha de nou al sistema d'instal·lació?"

#. type: Content of: <chapter><section><para>
#: en/installing.dbk:35
msgid ""
"There has been a lot of development on the Debian Installer since its "
"previous official release with &debian; &oldrelease;, resulting in improved "
"hardware support and some exciting new features or improvements."
msgstr ""
"S'ha fet molt desenvolupament a l'instal·lador de Debian des de el seu "
"anterior llançament oficial a &debian; &oldrelease; amb el resultat que "
"s'han aconseguit millores tant al suport de maquinari com noves i excitants "
"característiques."

#. type: Content of: <chapter><section><para>
#: en/installing.dbk:41
msgid ""
"If you are interested in an overview of the detailed changes since "
"&oldreleasename;, please check the release announcements for the "
"&releasename; beta and RC releases available from the Debian Installer's "
"<ulink url=\"&url-installer-news;\">news history</ulink>."
msgstr ""
"Si teniu interès en un visió general dels canvis detallats produïts des de "
"&oldreleasename;, comproveu els anuncis de llançament de la beta de "
"&releasename; i els llançaments RC que trobareu a <ulink url=\"&url-"
"installer-news;\">l'historial de notícies</ulink> de l'instal·lador de "
"Debian."

#. type: Content of: <chapter><section><section><title>
#: en/installing.dbk:120
msgid "Automated installation"
msgstr "Instal·lació automatitzada"

#. type: Content of: <chapter><section><section><para>
#: en/installing.dbk:122
msgid ""
"Some changes mentioned in the previous section also imply changes in the "
"support in the installer for automated installation using preconfiguration "
"files.  This means that if you have existing preconfiguration files that "
"worked with the &oldreleasename; installer, you cannot expect these to work "
"with the new installer without modification."
msgstr ""
"Alguns dels canvis que s'han anomenat en la secció anterior impliquen canvis "
"en el suport de l'instal·lador per instal·lacions automàtiques amb fitxers "
"de configuració prèvia. Açò vol dir que si ja teníeu fitxers de configuració "
"prèvia de la instal·lació de &oldreleasename;, no espereu que funcionen en "
"el nou instal·lador sense cap modificació."

#. type: Content of: <chapter><section><section><para>
#: en/installing.dbk:129
msgid ""
"The <ulink url=\"&url-install-manual;\">Installation Guide</ulink> has an "
"updated separate appendix with extensive documentation on using "
"preconfiguration."
msgstr ""
"La <ulink url=\"&url-install-manual;\">guia d'instal·lació</ulink> té un "
"apèndix separat amb una extensa documentació de com utilitzar la "
"configuració prèvia."

#. type: Content of: <chapter><section><title>
#: en/installing.dbk:138
#, fuzzy
#| msgid "Automated installation"
msgid "Cloud installations"
msgstr "Instal·lació automatitzada"

#. type: Content of: <chapter><section><para>
#: en/installing.dbk:140
msgid ""
"The <ulink url=\"&url-cloud-team;\">cloud team</ulink> publishes Debian "
"bullseye for several popular cloud computing services including:"
msgstr ""

#. type: Content of: <chapter><section><para><itemizedlist><listitem><para>
#: en/installing.dbk:147
msgid "OpenStack"
msgstr ""

#. type: Content of: <chapter><section><para><itemizedlist><listitem><para>
#: en/installing.dbk:152
msgid "Amazon Web Services"
msgstr ""

#. type: Content of: <chapter><section><para><itemizedlist><listitem><para>
#: en/installing.dbk:157
msgid "Microsoft Azure"
msgstr ""

#. type: Content of: <chapter><section><para>
#: en/installing.dbk:164
msgid ""
"Cloud images provide automation hooks via cloud-init and prioritize fast "
"instance startup using specifically optimized kernel packages and grub "
"configurations.  Images supporting different architectures are provided "
"where appropriate and the cloud team endeavors to support all features "
"offered by the cloud service."
msgstr ""

#. type: Content of: <chapter><section><para>
#: en/installing.dbk:173
msgid ""
"More details are available at <ulink url=\"&url-cloud;\">cloud.debian.org</"
"ulink> and <ulink url=\"&url-cloud-wiki;\">on the wiki</ulink>."
msgstr ""

#. type: Content of: <chapter><section><title>
#: en/installing.dbk:180
msgid "Container and Virtual Machine images"
msgstr ""

#. type: Content of: <chapter><section><para>
#: en/installing.dbk:182
msgid ""
"Multi-architecture Debian bullseye container images are available on <ulink "
"url=\"&url-docker-hub;\">Docker Hub</ulink>.  In addition to the standard "
"images, a <quote>slim</quote> variant is available that reduces disk usage."
msgstr ""

#. type: Content of: <chapter><section><para>
#: en/installing.dbk:188
msgid ""
"Virtual machine images for the Hashicorp Vagrant VM manager are published to "
"<ulink url=\"&url-vagrant-cloud;\">Vagrant Cloud</ulink>."
msgstr ""

#~ msgid ""
#~ "Most notably there is the initial support for UEFI Secure Boot (see <xref "
#~ "linkend=\"secure-boot\"/>), which has been added to the installation "
#~ "images."
#~ msgstr ""
#~ "Sobretot hi ha el suport inicial per l'arrancada segura amb UEFI (vegeu "
#~ "<xref linkend=\"secure-boot\"/>), que s'ha afegit a les imatges "
#~ "d'instal·lació."

#, fuzzy
#~ msgid "Major changes"
#~ msgstr "Canvis principals"

#, fuzzy
#~ msgid "Dropped platforms"
#~ msgstr "Plataformes no suportades"

#, fuzzy
#~ msgid ""
#~ "Support for the Alpha ('alpha'), ARM ('arm') and HP PA-RISC ('hppa')  "
#~ "architectures has been dropped from the installer.  The 'arm' "
#~ "architecture is obsoleted by the ARM EABI ('armel') port."
#~ msgstr ""
#~ "Suport per les arquitectures Alpha ('alpha'), ARM ('arm') i HP PA-RISC "
#~ "('hppa')  ha sigut tret del instal·lador oficial. L'arquitectura 'arm' es "
#~ "reemplaçada pel port ARM EABI ('armel')."

#, fuzzy
#~ msgid "Support for kFreeBSD"
#~ msgstr "Suport per kFreeBSD"

#, fuzzy
#~ msgid ""
#~ "The installer can be used to install the kFreeBSD instead of the Linux "
#~ "kernel and test the technology preview. To use this feature the "
#~ "appropriate installation image (or CD/DVD set) has to be used."
#~ msgstr ""
#~ "L'instal·lador pot fer-se servir per instal·lar el kernel de kFreeBSD en "
#~ "comptes del de Linux i provar aquesta tecnologia. Per fer servir aquesta "
#~ "opció s'ha de fer servir l'imatge (o conjunt de CD/DVD) apropiada per "
#~ "l'instal·lació."

#, fuzzy
#~ msgid "New supported platforms"
#~ msgstr "Plataformes noves suportades"

#, fuzzy
#~ msgid "The installation system now supports the following platforms:"
#~ msgstr "El sistema d'instal·lació ara suporta les següents plataformes:"

#, fuzzy
#~ msgid "Intel Storage System SS4000-E"
#~ msgstr "Intel Storage System SS4000-E"

#, fuzzy
#~ msgid "Help during the installation process"
#~ msgstr "Ajuda durant el procés d'instal·lació"

#, fuzzy
#~ msgid ""
#~ "The dialogs presented during the installation process now provide help "
#~ "information. Although not currently used in all dialogs, this feature "
#~ "would be increasingly used in future releases. This will improve the user "
#~ "experience during the installation process, especially for new users."
#~ msgstr ""
#~ "Els diàlegs presentats durant el procés d'instal·lació ara proporcionen "
#~ "informació d'ajuda. No obstant això no es fa servir a tots els diàlegs, "
#~ "aquesta característica es farà servir més encara a propers llançaments.  "
#~ "Això millora l'experiència d'usuari durant el procés d'instal·lació, "
#~ "especialment per nous usuaris."

#, fuzzy
#~ msgid "Installation of Recommended packages"
#~ msgstr "Instal·lació de paquets recomanats"

#, fuzzy
#~ msgid ""
#~ "The installation system will install all recommended packages by default "
#~ "throughout the process except for some specific situations in which the "
#~ "general setting gives undesired results."
#~ msgstr ""
#~ "El sistema d'instal·lació instal·larà tots els paquets recomanats per "
#~ "defecte durant el procés d'instal·lació, a excepció d'algunes situacions "
#~ "específiques en les quals la configuració general dóna resultats no "
#~ "desitjats."

#, fuzzy
#~ msgid "Automatic installation of hardware-specific packages"
#~ msgstr "Instal·lació automàtica de paquets depenent del maquinari"

#, fuzzy
#~ msgid ""
#~ "The system will automatically select for installation hardware-specific "
#~ "packages when they are appropriate. This is achieved through the use of "
#~ "<literal>discover-pkginstall</literal> from the <systemitem role=\"package"
#~ "\">discover</systemitem> package."
#~ msgstr ""
#~ "El sistema automàticament seleccionarà per instal·lació paquets que son "
#~ "específics al maquinari quan siga apropiat. Això es aconseguit a través "
#~ "de l'ús de <literal>discover-pkginstall</literal> al paquet <systemitem "
#~ "role=\"package\">discover</systemitem>."

#, fuzzy
#~ msgid "Support for installation of previous releases"
#~ msgstr "Suport per l'instal·lació de llançaments anteriors"

#, fuzzy
#~ msgid ""
#~ "The installation system can be also used for the installation of previous "
#~ "release, such as &oldreleasename;."
#~ msgstr ""
#~ "El sistema d'instal·lació pot fer-se servir per a l'instal·lació de "
#~ "llançaments anteriors, com &oldreleasename;."

#, fuzzy
#~ msgid "Improved mirror selection"
#~ msgstr "Millora de la selecció de rèpliques"

#, fuzzy
#~ msgid ""
#~ "The installation system provides better support for installing both "
#~ "&releasename; as well as &oldreleasename; and older releases (through the "
#~ "use of archive.debian.org). In addition, it will also check that the "
#~ "selected mirror is consistent and holds the selected release."
#~ msgstr ""
#~ "El sistema d'instal·lació proporciona un millor suport per instal·lar "
#~ "ambdós &releasename; així com &oldreleasename; i llançaments més antics "
#~ "(a través de l'ús de archive.debian.org). Addicionalment, també es "
#~ "comprovarà que la rèplica del arxiu seleccionada es consistent i que "
#~ "suporta el llançament seleccionat."

#, fuzzy
#~ msgid "Changes in partitioning features"
#~ msgstr "Canvis en les característiques de particionament"

#, fuzzy
#~ msgid ""
#~ "This release of the installer supports the use of the ext4 file system "
#~ "and it also simplifies the creation of RAID, LVM and crypto protected "
#~ "partitioning systems. Support for the reiserfs file system is no longer "
#~ "included by default, although it can be optionally loaded."
#~ msgstr ""
#~ "Aquesta versió de l'instal·lador dóna suport per a l'ús del sistema "
#~ "d'arxiu ext4 i també simplifica la creació de RAID, LVM i sistemes "
#~ "protegits per particionat xifrat.  Suport pel sistema d'arxiu reiserfs ja "
#~ "no és inclòs per defecte, tot i que pot ser opcionalment carregat."

#, fuzzy
#~ msgid "Support for loading firmware debs during installation"
#~ msgstr "Suport per la càrrega de firmware en el moment de la instal·lació"

#, fuzzy
#~ msgid ""
#~ "It is now possible to load firmware package files from the installation "
#~ "media in addition to removable media, allowing the creation of PXE images "
#~ "and CDs/DVDs with included firmware packages."
#~ msgstr ""
#~ "Ara es possible carregar paquets de firmware des dels mitjans "
#~ "d'instal·lació a més dels mitjans desmuntables, permetent la creació "
#~ "d'imatges PXE i CDs/DVDs amb paquets de firmware inclosos."

#, fuzzy
#~ msgid ""
#~ "Starting with Debian &release;, non-free firmware has been moved out of "
#~ "main.  To install Debian on hardware that needs non-free firmware, you "
#~ "can either provide the firmware yourself during installation or use pre-"
#~ "made non-free CDs/DVDs which include the firmware. See the <ulink url="
#~ "\"http://www.debian.org/distrib\">Getting Debian section</ulink> on the "
#~ "Debian website for more information."
#~ msgstr ""
#~ "Començant amb &debian; &release;, el firmware que no es lliure ha estat "
#~ "mogut fora dels repositoris principals.  Per instal·lar &debian; en "
#~ "maquinari que necessita firmware no lliure, pots proporcionar el firmware "
#~ "durant l'instal·lació o fer servir els CDs/DVDs que inclouen el firmware "
#~ "no lliure. Veure <ulink url=\"http://www.debian.org/distrib\">Aconseguint "
#~ "Debian</ulink> en la pàgina web de &debian; per més informació."

#, fuzzy
#~ msgid "New languages"
#~ msgstr "Noves llengües"

#, fuzzy
#~ msgid ""
#~ "Thanks to the huge efforts of translators, &debian; can now be installed "
#~ "in 70 languages.  This is seven more languages than in &oldreleasename;.  "
#~ "Most languages are available in both the text-based installation user "
#~ "interface and the graphical user interface, while some are only available "
#~ "in the graphical user interface."
#~ msgstr ""
#~ "Gràcies al titànic esforç dels traductors, &debian; ara disposa de 70 "
#~ "llengües.  Això és set més llengües que en &oldreleasename;.  La majoria "
#~ "de llengües estan disponibles tant en l'interfície d'usuari basada en "
#~ "text com en l'interfície d'usuari gràfica, mentre alguns són únicament "
#~ "disponibles en l'interfície d'usuari gràfica."

#, fuzzy
#~ msgid "Languages added in this release include:"
#~ msgstr "Les llengües afegides en aquest llançament inclouen:"

#, fuzzy
#~ msgid ""
#~ "Asturian, Estonian, Icelandic, Kazakh and Persian have been added to the "
#~ "graphical and text-based installer."
#~ msgstr ""
#~ "Bable, Estonià, Islandès, Kazakh i Persa han estat afegits tant a "
#~ "l'instal·lador basat en text com al gràfic."

#, fuzzy
#~ msgid ""
#~ "Kannada, Lao, Sinhala and Telugu have been added to the graphical "
#~ "installer."
#~ msgstr ""
#~ "Kannadai, Lao, Sinhala i Telugu han estat afegits a l'instal·lador gràfic."

#, fuzzy
#~ msgid ""
#~ "Thai, previously available only in the graphical user interface, is now "
#~ "available also in the text-based installation user interface too."
#~ msgstr ""
#~ "Tailandès, anteriorment disponible únicament en l'interfície d'usuari "
#~ "gràfica, és ara disponible també en l'interfície d'usuari basada en text."

#, fuzzy
#~ msgid ""
#~ "Due to the lack of translation updates two languages were dropped in this "
#~ "release: Wolof and Welsh."
#~ msgstr ""
#~ "A causa de la manca de traducció actualitzada dues llengües van ser "
#~ "deixades caure en aquest llançament: Wolof i Gal·lès."

#, fuzzy
#~ msgid ""
#~ "The languages that can only be selected using the graphical installer as "
#~ "their character sets cannot be presented in a non-graphical environment "
#~ "are: Amharic, Bengali, Dzongkha, Gujarati, Hindi, Georgian, Kannada, "
#~ "Khmer, Malayalam, Marathi, Nepali, Punjabi, Tamil and Telugu."
#~ msgstr ""
#~ "Les llengües que només poden ser seleccionat utilitzant l'instal·lador "
#~ "gràfic, ja que el seu conjunts de caràcters no poden ser presentats en un "
#~ "entorn no gràfic són: Amharic, Bengali, Dzongkha, Gujarati, Hindi, "
#~ "Georgian, Kannada, Khmer, Malayalam, Marathi, Nepali, Punjabi, Tamil i "
#~ "Telugu."

#, fuzzy
#~ msgid "Improved localisation selection"
#~ msgstr "Selecció de localització millorada"

#, fuzzy
#~ msgid ""
#~ "The selection of localisation-related values (language, location and "
#~ "locale settings) is now less interdependent and more flexible. Users will "
#~ "be able to customize the system to their localisation needs more easily "
#~ "while still make it comfortable to use for users that want to select the "
#~ "locale most common for the country they reside in."
#~ msgstr ""
#~ "La selecció de valors relacionats amb la localització (llengua, ubicació "
#~ "i opcions locals)  és ara menys interdependent i més flexible. Els "
#~ "usuaris seran capaços de fer-se el sistema a la seua mida més fàcilment "
#~ "en referència a la seva localització mentre encara es còmode d'utilitzar "
#~ "per usuaris que volen seleccionar la localització pel país on ells "
#~ "resideixen."

#, fuzzy
#~ msgid ""
#~ "Additionally, the consequences of localisation choices (such as timezone, "
#~ "keymap and mirror selection) are now more obvious to the user."
#~ msgstr ""
#~ "A més, les eleccions de localització (com zona horària, mapa de teclat, i "
#~ "selecció de rèplica) és ara més obvi a l'usuari."

#, fuzzy
#~ msgid "Live system installation"
#~ msgstr "Instal·lació amb sistema autònom"

#, fuzzy
#~ msgid ""
#~ "The installer now supports live systems in two ways. First, an installer "
#~ "included on live system media can use the contents of the live system in "
#~ "place of the regular installation of the base system. Second, the "
#~ "installer may now be launched while running the live system, allowing the "
#~ "user to do other things with the live system during the install. Both "
#~ "features are built into the Debian Live images offered at <ulink url="
#~ "\"http://cdimage.debian.org/\" />."
#~ msgstr ""
#~ "L'instal·lador ara suporta sistemes autònoms de dos maneres diferents. "
#~ "Primer, un instal·lador inclòs al sistema autònom pot fer servir els "
#~ "continguts del sistema autònom en lloc de la instal·lació normal del "
#~ "sistema base. Segon, l'instal·lador pot ara executar-se mentre el sistema "
#~ "autònom està en execució, permitent l'usuari fer altres coses amb el "
#~ "sistema autònom durant l'instal·lació. Ambdues característiques formen "
#~ "part de les imatges de Debian Live oferides a <ulink url=\"http://cdimage."
#~ "debian.org/\" />."
